<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('omise_api');
		$this->user_token = '';
		$this->card_token = '';
		$this->chrg_token = '';
	}

	public function index()
	{
		$this->load->view('test_omise');
	}

	public function add_customer()
	{
		$customer_omise = $this->omise_api->customer_crate(date('i:s'));
		echo json_encode($customer_omise);
	}

	public function get_token(){
		$token = $this->input->post('omise_token');
		$this->omise_api->customer_add_card($this->user_token,$token);
		$customer_omise = $this->omise_api->customer_get($this->user_token);
		echo json_encode($customer_omise);
	}

	public function charges(){
		$fee = rand(2500,3000);
		$charge_return = $this->omise_api->charges($this->user_token, $this->card_token, $fee );
		echo json_encode($charge_return);
	}

	public function refund(){
		$refund = 2000;
		$return = $this->omise_api->refund($this->chrg_token,$refund);
		echo json_encode($return);
	}
}
