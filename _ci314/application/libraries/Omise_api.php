<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/omise/OmiseAccount.php';
require_once dirname(__FILE__).'/omise/OmiseBalance.php';
require_once dirname(__FILE__).'/omise/OmiseCard.php';
require_once dirname(__FILE__).'/omise/OmiseCardList.php';
require_once dirname(__FILE__).'/omise/OmiseDispute.php';
require_once dirname(__FILE__).'/omise/OmiseEvent.php';
require_once dirname(__FILE__).'/omise/OmiseForex.php';
require_once dirname(__FILE__).'/omise/OmiseToken.php';
require_once dirname(__FILE__).'/omise/OmiseCharge.php';
require_once dirname(__FILE__).'/omise/OmiseCustomer.php';
require_once dirname(__FILE__).'/omise/OmiseOccurrence.php';
require_once dirname(__FILE__).'/omise/OmiseRefund.php';
require_once dirname(__FILE__).'/omise/OmiseRefundList.php';
require_once dirname(__FILE__).'/omise/OmiseSearch.php';
require_once dirname(__FILE__).'/omise/OmiseSchedule.php';
require_once dirname(__FILE__).'/omise/OmiseScheduler.php';
require_once dirname(__FILE__).'/omise/OmiseSource.php';
require_once dirname(__FILE__).'/omise/OmiseTransfer.php';
require_once dirname(__FILE__).'/omise/OmiseTransaction.php';
require_once dirname(__FILE__).'/omise/OmiseRecipient.php';
require_once dirname(__FILE__).'/omise/OmiseLink.php';

class Omise_api {

	var $_api_url = 'https://api.omise.co/';
	var $pkey, $skey, $user_id;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('omisemodel');
		$this->user_id = 0;
		$this->pkey = $this->CI->config->item('pkey');
		$this->skey = $this->CI->config->item('skey');

	}

	public function setUserID($user_id){

		$this->user_id = $user_id;

	}

	public function charges($cust_token, $card_token, $amount, $description = null, $currency = 'thb'){

		try{

			$charge = OmiseCharge::create(array(
				'amount' => $amount,
				'currency' => $currency,
				'customer' => $cust_token,
				'card' => $card_token
			));

			return $this->return_array($charge,true);

		}catch(Exception $e){

			//$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
		
	}

	public function get_charges($charge_token){

		try{

			$charge = OmiseCharge::retrieve($charge_token,$this->pkey,$this->skey,true);

			return $this->return_array($charge,true);

		}catch(Exception $e){

			//$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
		
	}

	public function refund($charge_token,$amount){

		try{

			$charge = OmiseCharge::retrieve($charge_token);

			$refund = $charge->refunds()->create(array('amount' => $amount));

			return array('status' => 'success');

		}catch(Exception $e){

			//$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
		
	}

	public function retrieve($charge_id = ''){
		try{
			$param = array(
				'from' => '2018-08-20 14:00:00',
				'to' => '2018-08-20 14:30:00'
			);
			$retrieve = OmiseCharge::retrieve($charge_id);
			return $this->return_array($retrieve);

		}catch(Exception $e){

			//$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
	}

	public function customer_crate($user_id = 0){
		try{
			$email = 'test_'.$user_id.'@test.nick';
			$desc = '('.$user_id.')';
			$options = array(
				'email' => $email,
				'description' => $desc
			);

			$customer = OmiseCustomer::create($options);
			return $this->return_array($customer);

		}catch(Exception $e){

			$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
	}

	public function customer_add_card($customer_token = '',$card_token = ''){
		try{
			$customer = OmiseCustomer::retrieve($customer_token);
			$customer->update(array(
				'card' => $card_token
			));
			return array('status' => 'success');

		}catch(Exception $e){

			//$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
	}

	public function customer_get($customer_token = ''){
		try{
			$customer = OmiseCustomer::retrieve($customer_token,$this->pkey,$this->skey,true);

			return $this->return_array($customer);

		}catch(Exception $e){

			//$this->CI->omisemodel->insert($this->user_id,$e->getMessage());
			return array('error' => $e->getMessage());

		}
	}
	
	private function return_array($json = '',$is_insert_db = false){
		if($is_insert_db){
			//$this->CI->omisemodel->insert($this->user_id,$json);
		}
		return json_decode($json,true);
	}
	

}